import 'package:my_movies_list/data/exeptions/comment_other_user.dart';
import 'package:my_movies_list/data/exeptions/tile_not_rate_.dart';
import 'package:my_movies_list/data/models/comment_model.dart';
import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/models/title_favorite_model.dart';
import 'package:my_movies_list/data/models/title_movies.dart';
import 'package:my_movies_list/data/repositories/services/http_service.dart';
import 'package:my_movies_list/data/repositories/user/user_repository_iinterface.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';

class TitleRepository implements TitleRepositoryInterface {
  final HttpService _httpService;
  final _baseUrl = 'https://xbfuvqstcb.execute-api.us-east-1.amazonaws.com/dev';

  TitleRepository(this._httpService);

  @override
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params}) async {
    String uri = '$_baseUrl/movies';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/tv/';
    var response = await _httpService.getRequest(uri, params: params);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(
          data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<TitleDetailModel?> getTitleDetail(int idTitleDetails,
      {bool isTvShow = false}) async {
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$idTitleDetails';
    var response = await _httpService.getRequest(uri);

    if (response.success) {
      return TitleDetailModel.fromJson(response.content!);
    } else {
      throw Exception(response.exception);
    }
  }

  @override
  Future<List<TitleModel>> getTitleRecommendation(int idTileRecommendation,
      {bool isTvShow = false}) async {
    final uri =
        '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$idTileRecommendation/recommendations';
    var response = await _httpService.getRequest(uri);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(
          data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<bool> removeComment(int titleId, int commentId,
      {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri =
        '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/$commentId/comment';

    var response = await _httpService.deleteRequest(uri, headers: headers);

    if (response.success) {
      return response.success;
    }

    if (response.statusCode == 403) {
      throw CommentOtherUserException();
    }

    throw Exception('Falha ao excluir comentário');
  }

  @override
  Future<bool> saveComment(int titleId, String comment,
      {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/comment';

    var response = await _httpService.postRequest(uri, {'comment': comment},
        headers: headers);
    return response.success;
  }

  @override
  Future<int> getTitleFavorite(int titleId, {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/rate';

    var response = await _httpService.getRequest(uri, headers: headers);
    if (response.success) {
      return response.content!['rate'];
    }

    if (response.statusCode == 404) {
      throw TitleNotRatedException();
    }

    throw Exception('Falha ao cadastrar o usuário');
  }

  @override
  Future<bool> saveTitleFavorite(int titleId, int rate,
      {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/rate';

    var response =
        await _httpService.postRequest(uri, {'rate': rate}, headers: headers);
    return response.success;
  }

  @override
  Future<List<TitleFavoriteModel>> getUserFavoriteTitleList(
      String? userId) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};

    if (userId == null) {
      var userModel = await getIt.get<UserRepositoryInterface>().getUser();
      userId = userModel!.id;
    }

    final uri = '$_baseUrl/users/$userId/titles-rated';

    var response = await _httpService.getRequest(uri, headers: headers);
    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleFavoriteModel>.from(
          data.map((json) => TitleFavoriteModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getPopularMovieList(
      {Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/movies/popular';
    var response = await _httpService.getRequest(uri, params: params);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(
          data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getUpcomingMovieList(
      {Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/movies/upcoming';
    var response = await _httpService.getRequest(uri, params: params);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(
          data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getPopularTvList(
      {Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/tv/popular';
    var response = await _httpService.getRequest(uri, params: params);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<TitleModel>.from(
          data.map((json) => TitleModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<List<CommentModel>> getTitleComments(int titleId,
      {bool isTvShow = false}) async {
    final token = await getIt.get<UserRepositoryInterface>().getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final uri = '$_baseUrl/${isTvShow ? 'tv' : 'movies'}/$titleId/comments';

    var response = await _httpService.getRequest(uri, headers: headers);
    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<CommentModel>.from(
          data.map((json) => CommentModel.fromJson(json)));
    }

    return [];
  }

  @override
  Future<int> getCountMovieList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/movies/';
    var response = await _httpService.getRequest(uri, params: params);

    if (response.success) {
      return response.content!['count'];
    }

    return 0;
  }
}
