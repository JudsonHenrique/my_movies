import 'package:my_movies_list/data/models/comment_model.dart';
import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/models/title_favorite_model.dart';
import 'package:my_movies_list/data/models/title_movies.dart';

abstract class TitleRepositoryInterface {
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getPopularMovieList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getUpcomingMovieList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params});

  Future<TitleDetailModel?> getTitleDetail(int idTitleDetails,
      {bool isTvShow = false});
  Future<List<TitleModel>> getPopularTvList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getTitleRecommendation(int idTileRecommendation,
      {bool isTvShow = false});

  Future<bool> saveComment(int titleId, String text, {bool isTvShow = false});

  Future<List<CommentModel>> getTitleComments(int titleId,
      {bool isTvShow = false});

  Future<bool> removeComment(int titleId, int commentId,
      {bool isTvShow = false});

  Future<bool> saveTitleFavorite(int titleId, int rate,
      {bool isTvShow = false});

  Future<int> getTitleFavorite(int titleId, {bool isTvShow = false});

  Future<List<TitleFavoriteModel>> getUserFavoriteTitleList(String? userId);

  Future<int> getCountMovieList({Map<String, dynamic>? params});
}
