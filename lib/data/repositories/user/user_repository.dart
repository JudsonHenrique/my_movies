import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:my_movies_list/data/exeptions/user_already_exists.dart';
import 'package:my_movies_list/data/exeptions/user_not_found.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/services/http_service.dart';
import 'package:my_movies_list/data/repositories/shared/string.dart';
import 'package:my_movies_list/data/repositories/user/user_repository_iinterface.dart';

class UserRepository implements UserRepositoryInterface {
  final HttpService _http;
  final _baseUrl = Strings.movie_api_url;
  final _secureStorage = const FlutterSecureStorage();
  UserModel? user;

  UserRepository(this._http);

  @override
  Future<String?> getToken() {
    return _secureStorage.read(key: 'AUTH_TOKEN');
  }

  @override
  Future<UserModel?> getUser() async {
    if (user == null) {
      final token = await getToken();
      final uri = '$_baseUrl/auth/me';
      final headers = {'Authorization': 'Bearer $token'};
      final response = await _http.getRequest(uri, headers: headers);
      user = UserModel.fromJson(response.content!);
    }

    return user;
  }

  @override
  Future<UserModel?> login(String email, String password) async {
    final uri = '$_baseUrl/auth/login';
    final body = {'email': email, 'password': password};
    final response = await _http.postRequest(uri, body);

    if (response.success) {
      var token = response.content!['token'];

      await saveToken(token);
      return getUser();
    }

    if (response.statusCode == 400) {
      throw UserNotFoundException();
    }

    throw Exception('Falha ao realizar login');
  }

  @override
  Future<UserModel?> register(
      String email, String password, String name) async {
    final uri = '$_baseUrl/auth/register';
    final body = {'email': email, 'password': password, 'name': name};

    var response = await _http.postRequest(uri, body);
    if (response.success) {
      var token = response.content!['token'];

      await saveToken(token);

      return getUser();
    }

    if (response.statusCode == 400) {
      throw UserAlredyExistsException();
    }

    throw Exception('Falha cadastrar usuário');
  }

  @override
  Future<void> saveToken(String token) async {
    await _secureStorage.write(key: 'AUTH_TOKEN', value: token);
  }

  @override
  Future<bool> isLogged() async {
    final token = await getToken();
    return token != null;
  }

  @override
  Future<void> clearSession() async {
    await _secureStorage.delete(key: 'AUTH_TOKEN');
  }

  @override
  Future<List<UserModel>> listUsers() async {
    final uri = '$_baseUrl/users';
    final token = await getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final response = await _http.getRequest(uri, headers: headers);

    if (response.success) {
      List<dynamic> data = response.content!['data'];
      return List<UserModel>.from(data.map((json) => UserModel.fromJson(json)));
    }

    return [];
  }
}
