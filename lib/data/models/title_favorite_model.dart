class TitleFavoriteModel {
  int id;
  String name;
  String originalName;
  String coverUrl;
  String posterUrl;
  int? rate;

  TitleFavoriteModel({
    required this.id,
    required this.name,
    required this.originalName,
    required this.coverUrl,
    required this.posterUrl,
    this.rate,
  });

  factory TitleFavoriteModel.fromJson(Map<String, dynamic> json) {
    return TitleFavoriteModel(
      id: json['id'],
      name: json['name'],
      originalName: json['original_name'],
      coverUrl: json['cover_url'],
      posterUrl: json['poster_url'],
      rate: json['rate'],
    );
  }
}
