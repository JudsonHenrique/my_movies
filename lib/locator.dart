import 'package:get_it/get_it.dart';
import 'package:my_movies_list/data/repositories/services/http_service.dart';
import 'package:my_movies_list/data/repositories/user/user_repository_iinterface.dart';
import 'package:my_movies_list/data/repositories/title/tilte_repository.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';

import 'data/repositories/user/user_repository.dart';

var getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton(() => HttpService());
  getIt.registerLazySingleton<TitleRepositoryInterface>(
      () => TitleRepository(getIt.get<HttpService>()));
  getIt.registerLazySingleton<UserRepositoryInterface>(
      () => UserRepository(getIt.get<HttpService>()));
}
