import 'package:flutter/material.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/favorite/favorite_page.dart';
import 'package:my_movies_list/ui/pages/home/home_page.dart';
import 'package:my_movies_list/ui/pages/login/login_page.dart';
import 'package:my_movies_list/ui/pages/register/registration_page.dart';
import 'package:my_movies_list/ui/pages/search/search_page.dart';
import 'package:my_movies_list/ui/pages/splash/splash_page.dart';
import 'package:my_movies_list/ui/pages/details/title_details_pages.dart';
import 'package:my_movies_list/ui/pages/user/user_page.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Movies List',
      debugShowCheckedModeBanner: false,
      initialRoute: SplashPage.name,
      routes: {
        LoginPage.name: (_) => const LoginPage(),
        HomePage.name: (_) => const HomePage(),
        SearchPage.name: (_) => const SearchPage(),
        TitleDetailsPage.name: (_) => const TitleDetailsPage(),
        RegistrationPage.name: (_) => const RegistrationPage(),
        FavoritePage.name: (_) => const FavoritePage(),
        UserPage.name: (_) => const UserPage(),
        SplashPage.name: (_) => const SplashPage(),
      },
    );
  }
}
