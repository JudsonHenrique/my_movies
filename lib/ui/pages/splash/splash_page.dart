import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/splash/splash_cubit.dart';
import 'package:my_movies_list/data/repositories/user/user_repository_iinterface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home/home_page.dart';
import 'package:my_movies_list/ui/pages/login/login_page.dart';

class SplashPage extends StatelessWidget {
  static const name = 'splash-page';

  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) =>
          SplashCubit(getIt.get<UserRepositoryInterface>())..checkUser(),
      child: const SplashView(),
    );
  }
}

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashCubit, SplashState>(
      listener: (BuildContext context, state) {
        if (state == SplashState.userLogged) {
          Navigator.pushReplacementNamed(context, HomePage.name);
        } else if (state == SplashState.userNotLogged) {
          Navigator.pushReplacementNamed(context, LoginPage.name);
        }
      },
      child: const Scaffold(
        body: Center(
          child: Icon(
            Icons.movie,
            size: 30.0,
          ),
        ),
      ),
    );
  }
}
