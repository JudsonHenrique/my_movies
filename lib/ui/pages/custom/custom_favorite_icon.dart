import 'package:flutter/material.dart';

class CustomIcon extends StatelessWidget {
  final bool? value;
  final Function(bool)? onChanged;

  const CustomIcon({
    required this.value,
    this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon((value != null && value!)
              ? Icons.thumb_up
              : Icons.thumb_up_alt_outlined),
          onPressed: () {
            if (onChanged != null) {
              onChanged!(true);
            }
          },
        ),
        IconButton(
          icon: Icon((value != null && !value!)
              ? Icons.thumb_down
              : Icons.thumb_down_alt_outlined),
          onPressed: () {
            if (onChanged != null) {
              onChanged!(false);
            }
          },
        ),
      ],
    );
  }
}
