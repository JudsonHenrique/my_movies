import 'package:flutter/material.dart';

class CustomLoadingButton extends StatelessWidget {
  final bool isLoading;
  final Widget child;
  final VoidCallback onPressed;
  const CustomLoadingButton({
    required this.child,
    required this.onPressed,
    this.isLoading = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: BorderSide(color: Theme.of(context).primaryColor, width: 2)),
      ),
      child: isLoading
          ? const Padding(
              padding: EdgeInsets.all(8.0),
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            )
          : child,
      onPressed: onPressed,
    );
  }
}
