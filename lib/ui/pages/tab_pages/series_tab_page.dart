import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/bloc/home/home_bloc.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';
import 'package:my_movies_list/ui/pages/tab_pages/tabs_title_view.dart';

class SeriesTabPage extends StatelessWidget {
  const SeriesTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titlesList = [
      TitleType('Série de comédia', {'genre': 35}, true),
      TitleType('Série de Animação', {'genre': 16}, true),
      TitleType('Série de Drama', {'genre': 18}, true),
      TitleType('Série de Documentário', {'genre': 99}, true),
    ];

    return BlocProvider(
      create: (_) => CatalogTitleCubit(getIt.get<TitleRepositoryInterface>())
        ..fetchTitles(titlesList),
      child: TabTitleView(titlesList),
    );
  }
}
