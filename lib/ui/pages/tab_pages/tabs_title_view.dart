import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/home/home_bloc.dart';
import 'package:my_movies_list/data/models/title_movies.dart';
import 'package:my_movies_list/ui/pages/custom/custom_image_network.dart';
import 'package:my_movies_list/ui/pages/custom/custom_title_carousel.dart';
import 'package:my_movies_list/ui/pages/details/title_details_pages.dart';

class TabTitleView extends StatelessWidget {
  final List<TitleType> titlesList;

  const TabTitleView(this.titlesList, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: titlesList
            .map(
              (t) => BlocBuilder<CatalogTitleCubit, TitleState>(
                buildWhen: (oldState, newState) {
                  if (newState is LoadingTitlesState) {
                    return newState.type.label == t.label;
                  } else if (newState is TitlesLoadedState) {
                    return newState.type.label == t.label;
                  }

                  return true;
                },
                builder: (context, state) {
                  if (state is LoadingTitlesState) {
                    return const CircularProgressIndicator();
                  } else if (state is TitlesLoadedState) {
                    return Carousel(
                      label: t.label,
                      children: state.titles
                          .map((t) =>
                              _buildTitleCard(title: t, context: context))
                          .toList(),
                    );
                  }

                  return Container();
                },
              ),
            )
            .toList(),
      ),
    );
  }

  Widget _buildTitleCard(
      {required TitleModel title, required BuildContext context}) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, TitleDetailsPage.name, arguments: {
          'id': title.id,
          'isTvShow': title.isTvShow,
        });
      },
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, TitleDetailsPage.name,
              arguments: {'id': title.id, 'is_tv_show': title.isTvShow});
        },
        child: Column(
          children: [
            GestureDetector(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Container(
                  width: 126,
                  height: 225,
                  margin: const EdgeInsets.only(left: 10.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blue, width: 1),
                      borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  child: Center(
                      child: CustomImageNetwork(
                          url: title.posterUrl, name: title.name)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
