import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/home/home_bloc.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';
import 'package:my_movies_list/ui/pages/tab_pages/tabs_title_view.dart';
import '../../../locator.dart';

class MainTabPage extends StatelessWidget {
  const MainTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const Padding(
      padding: EdgeInsets.only(bottom: 20),
    );
    final titlesList = [
      TitleType('Série de comédia', {'genre': 35}, true),
      TitleType('Filme de comédia', {'genre': 35}, false),
      TitleType('Filme de ação', {'genre': 28}, false),
      TitleType('Filme de suspense', {'genre': 9648}, false),
    ];

    return BlocProvider(
      create: (_) => CatalogTitleCubit(getIt.get<TitleRepositoryInterface>())
        ..fetchTitles(titlesList),
      child: TabTitleView(titlesList),
    );
  }
}
