import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/home/home_bloc.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';
import 'package:my_movies_list/ui/pages/tab_pages/tabs_title_view.dart';

import '../../../locator.dart';

class MoviesTabPage extends StatelessWidget {
  const MoviesTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titlesList = [
      TitleType('Filme de terror', {'genre': 27}, false),
      TitleType('Filme de romance', {'genre': 10749}, false),
      TitleType('Filme de comédia', {'genre': 35}, false),
      TitleType('Filme de suspense', {'genre': 9648}, false),
      TitleType('Filme de ação', {'genre': 28}, false),
    ];

    return BlocProvider(
      create: (_) => CatalogTitleCubit(getIt.get<TitleRepositoryInterface>())
        ..fetchTitles(titlesList),
      child: TabTitleView(titlesList),
    );
  }
}
