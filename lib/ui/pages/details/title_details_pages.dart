import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/details/title_comment_cubit.dart';
import 'package:my_movies_list/bloc/details/title_detail_cubit.dart';
import 'package:my_movies_list/bloc/details/title_rate_cubit.dart';
import 'package:my_movies_list/bloc/details/title_recommendation_cubit.dart';
import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/custom/custom_favorite_icon.dart';
import 'package:my_movies_list/ui/pages/custom/custom_image_network.dart';
import 'package:my_movies_list/ui/pages/custom/custom_loadin_button.dart';
import 'package:my_movies_list/ui/pages/custom/custom_text_form_field.dart';
import 'package:my_movies_list/ui/pages/custom/custom_title_carousel.dart';

class TitleDetailsPage extends StatelessWidget {
  static const name = 'title-details-page';

  const TitleDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final arguments = (ModalRoute.of(context)!.settings.arguments as Map);
    final titleId = arguments['id'] as int;
    final isTvShow = arguments['is_tv_show'] as bool;

    return MultiBlocProvider(
      providers: [
        BlocProvider<TitleDetailCubit>(
          create: (context) =>
              TitleDetailCubit(getIt.get<TitleRepositoryInterface>())
                ..getTitleDetail(titleId, isTvShow: isTvShow),
        ),
        BlocProvider<TitleRateCubit>(
          create: (context) =>
              TitleRateCubit(getIt.get<TitleRepositoryInterface>())
                ..getTitleRate(titleId, isTvShow: isTvShow),
        ),
        BlocProvider<TitleRecommendationCubit>(
          create: (context) =>
              TitleRecommendationCubit(getIt.get<TitleRepositoryInterface>())
                ..getTitleRecommendation(titleId, isTvShow: isTvShow),
        ),
        BlocProvider<TitleCommentCubit>(
          create: (context) =>
              TitleCommentCubit(getIt.get<TitleRepositoryInterface>())
                ..getTitleComments(titleId, isTvShow: isTvShow),
        ),
      ],
      child: TitleDetailsView(isTvShow: isTvShow, titleId: titleId),
    );
  }
}

class TitleDetailsView extends StatelessWidget {
  final int titleId;
  final bool isTvShow;

  TitleDetailsView({
    Key? key,
    required this.titleId,
    required this.isTvShow,
  }) : super(key: key);

  final _commentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<TitleDetailCubit, TitleDetailState>(
        builder: (context, state) {
          if (state is ProcessingTitleDetailState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          if (state is FailTitleDetailState) {
            return const Center(
              child: Text('Falha ao carregar os detalhes do título'),
            );
          }

          if (state is SuccessTitleDetailState) {
            var titleDetail = state.detail;

            return Column(
              children: [
                Image.network(titleDetail.coverUrl),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: _buildDetailTitle(
                        context: context,
                        titleDetail: titleDetail,
                        isTvShow: isTvShow),
                  ),
                )
              ],
            );
          } else {
            return const SizedBox();
          }
        },
      ),
    );
  }

  Widget _buildDetailTitle(
      {required BuildContext context,
      required TitleDetailModel titleDetail,
      required bool isTvShow}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              titleDetail.name,
              style: const TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        const SizedBox(height: 20.0),
        Expanded(
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              const Text(
                'Sinopse:',
                style: TextStyle(fontWeight: FontWeight.w700),
              ),
              const SizedBox(height: 5.0),
              Text(
                titleDetail.overview,
                style: const TextStyle(fontSize: 15.0),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Text('Duração: ${titleDetail.runtime} min'),
              ),
              const SizedBox(height: 10.0),
              _buildTypesCategories(titleDetail.genres),
              const SizedBox(height: 5.0),
              _buildFavoritIcon(),
              const SizedBox(height: 10.0),
              _buildTitelRecommendations(),
              _buildComments(),
              BlocBuilder<TitleCommentCubit, TitleCommentState>(
                  builder: (context, state) {
                return Row(
                  children: [
                    Expanded(
                      child: CustomTextFormField(
                        controller: _commentController,
                        label: 'Comentar',
                        obscure: false,
                        icon: const Icon(Icons.comment),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: CustomLoadingButton(
                        child: const Text('Enviar'),
                        onPressed: () async {
                          context.read<TitleCommentCubit>().saveComment(
                              titleDetail.id, _commentController.text,
                              isTvShow: isTvShow);
                          _commentController.clear();
                        },
                      ),
                    ),
                  ],
                );
              })
            ]),
          ),
        ),
      ],
    );
  }

  Widget _buildTypesCategories(List<String> categories) {
    return Carousel(
      children: categories
          .map(
            (String e) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2.5),
              child: Chip(
                label: Text(e),
              ),
            ),
          )
          .toList(),
    );
  }

  Widget _buildFavoritIcon() {
    return BlocBuilder<TitleRateCubit, TitleRateState>(
      builder: (context, state) {
        if (state is ProcessingTitleRateState) {
          return const Center(child: CircularProgressIndicator());
        }

        return CustomIcon(
          value: state is SuccessTitleRateState ? (state.rate) : null,
          onChanged: (bool value) {
            context
                .read<TitleRateCubit>()
                .saveTitleRate(titleId, value ? 1 : -1, isTvShow: isTvShow);
          },
        );
      },
    );
  }

  Widget _buildTitelRecommendations() {
    return BlocBuilder<TitleRecommendationCubit, TitleRecommendationState>(
      builder: (context, state) {
        if (state is ProcessingTitleRecommendationState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        if (state is SuccessTitleRecommendationState) {
          var recommendations = state.recommendations;
          return Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Carousel(
              label: 'Recomendados',
              children: recommendations
                  .map(
                    (e) => GestureDetector(
                      onTap: () => Navigator.pushNamed(
                          context, TitleDetailsPage.name,
                          arguments: {'id': e.id, 'is_tv_show': e.isTvShow}),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Container(
                          width: 126,
                          height: 225,
                          margin: const EdgeInsets.only(left: 10.0),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blue, width: 1),
                              borderRadius: const BorderRadius.only(
                                  bottomLeft: Radius.circular(20),
                                  bottomRight: Radius.circular(20))),
                          child: Center(
                              child: CustomImageNetwork(
                                  url: e.posterUrl, name: e.name)),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  Widget _buildComments() {
    return BlocBuilder<TitleCommentCubit, TitleCommentState>(
      builder: (context, state) {
        if (state is ProcessingTitleCommentState) {
          return const Center(child: CircularProgressIndicator());
        }

        if (state is CommentByOtherUserTitleCommentState) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text(
                  'Não é permitido excluir um comentário feito por outro usuário')));
        }

        if (state is FailTitleCommentState) {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text(
                  'Não é permitido excluir um comentário feito por outro usuário')));
        }

        if (state is SuccessTitleCommentState) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Text(
                  'Comentários',
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              ...state.comments
                  .take(5)
                  .map(
                    (e) => e.text.isNotEmpty
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: ListTile(
                                  title: Text(
                                    e.text,
                                    maxLines: 1,
                                    overflow: TextOverflow.clip,
                                  ),
                                  // subtitle: Text(
                                  //   DateFormat('dd/MM/yyyy hh:mm:ss').format(e.date),
                                  // ),
                                ),
                              ),
                              IconButton(
                                onPressed: () => context
                                    .read<TitleCommentCubit>()
                                    .removeComment(titleId, e.id,
                                        isTvShow: isTvShow),
                                icon: const Icon(Icons.delete_outline_outlined),
                              ),
                            ],
                          )
                        : const SizedBox(),
                  )
                  .toList(),
              if (state.comments.length > 5)
                TextButton(
                  onPressed: () {},
                  child: Text(
                    'Ver todos os ${state.comments.length} comentários',
                    textAlign: TextAlign.start,
                  ),
                ),
            ],
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }
}
