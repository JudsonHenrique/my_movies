import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/login/login_cubit.dart';
import 'package:my_movies_list/data/repositories/user/user_repository_iinterface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/custom/custom_loadin_button.dart';
import 'package:my_movies_list/ui/pages/custom/custom_text_form_field.dart';
import 'package:my_movies_list/ui/pages/home/home_page.dart';
import 'package:my_movies_list/ui/pages/register/registration_page.dart';

class LoginPage extends StatelessWidget {
  static const name = 'login-page';

  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => LoginCubit(getIt.get<UserRepositoryInterface>()),
      child: LoginView(),
    );
  }
}

class LoginView extends StatelessWidget {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (BuildContext context, state) {
        if (state == LoginState.success) {
          Navigator.pushReplacementNamed(context, HomePage.name);
        }
      },
      child: SafeArea(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Center(
                      child: Text(
                        'Informe suas credencias para começar!',
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w300,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                    const SizedBox(height: 25.0),
                    CustomTextFormField(
                      label: 'Email',
                      icon: const Icon(Icons.lock),
                      obscure: false,
                      textInputType: TextInputType.emailAddress,
                      controller: _emailController,
                    ),
                    const SizedBox(height: 20.0),
                    CustomTextFormField(
                      label: 'Senha',
                      icon: const Icon(Icons.lock),
                      controller: _passwordController,
                      obscure: true,
                    ),
                    const SizedBox(height: 25.0),
                    BlocBuilder<LoginCubit, LoginState>(
                        builder: (context, state) {
                      return Visibility(
                        visible: state == LoginState.loginFailed ||
                            state == LoginState.userNotFound,
                        child: Text(
                          state == LoginState.userNotFound
                              ? 'Usuário não encontrado'
                              : 'Falha ao realizar login',
                          style: const TextStyle(color: Colors.red),
                        ),
                      );
                    }),
                    BlocBuilder<LoginCubit, LoginState>(
                        builder: (context, state) {
                      return CustomLoadingButton(
                        isLoading: state == LoginState.processingLogin,
                        onPressed: () {
                          context.read<LoginCubit>().login(
                              _emailController.text, _passwordController.text);
                        },
                        child: const Text('Entrar'),
                      );
                    }),
                    const SizedBox(height: 15.0),
                    Text(
                      'OU',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12.0,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, RegistrationPage.name);
                      },
                      child: const Text('Criar minha conta'),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
