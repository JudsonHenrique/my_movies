import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/favorite/favorite_title_list.dart';
import 'package:my_movies_list/bloc/details/title_rate_cubit.dart';
import 'package:my_movies_list/data/models/title_favorite_model.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/custom/custom_favorite_icon.dart';
import 'package:my_movies_list/ui/pages/custom/custom_image_network.dart';
import 'package:my_movies_list/ui/pages/search/search_page.dart';

class FavoritePage extends StatelessWidget {
  static const name = 'favorite-page';

  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = (ModalRoute.of(context)!.settings.arguments as Map? ?? {});
    final String? userId = args['user_id'];
    final String? userName = args['user_name'];

    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (context) =>
                RatedTitleListCubit(getIt.get<TitleRepositoryInterface>())
                  ..getUserFavoriteTitleList(userId)),
        BlocProvider(
            create: (context) =>
                TitleRateCubit(getIt.get<TitleRepositoryInterface>())),
      ],
      child: FavoritePageView(userId: userId, userName: userName),
    );
  }
}

class FavoritePageView extends StatelessWidget {
  final String? userId;
  final String? userName;
  const FavoritePageView({Key? key, this.userId, this.userName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.arrow_back),
            ),
            title: const Text('Meu catálogo de filmes e séries'),
            bottom: const TabBar(
              tabs: [
                Tab(
                    child: Text(
                  'Meus Filmes Favoritos',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                )),
              ],
            ),
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, SearchPage.name);
                },
                icon: const Icon(Icons.search),
              )
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BlocBuilder<RatedTitleListCubit, RatedTitleListState>(
                    builder: (context, state) {
                      if (state is ProcessingRatedTitleListState) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }

                      if (state is FailRatedTitleListState) {
                        return const Center(
                          child: Text('Falha ao carregar os títulos avaliados'),
                        );
                      }

                      if (state is SuccessRatedTitleListState) {
                        return Wrap(
                          children: state.ratings
                              .map((e) => _buildTitleCard(
                                  context: context,
                                  title: e,
                                  canRate: userId == null))
                              .toList(),
                        );
                      } else {
                        return const SizedBox();
                      }
                    },
                  ),
                ],
              ),
            ),
          )),
    );
  }

  Widget _buildTitleCard(
      {required TitleFavoriteModel title,
      required BuildContext context,
      bool canRate = false}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0, right: 10, left: 10, top: 20),
      child: GestureDetector(
        child: Padding(
          padding: const EdgeInsets.only(
            bottom: 10,
          ),
          child: Container(
            width: 150,
            height: 270,
            margin: const EdgeInsets.only(left: 10.0),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.blue, width: 1),
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: CustomImageNetwork(
                    url: title.posterUrl,
                    name: title.name,
                    height: 180,
                  ),
                ),
                BlocBuilder<TitleRateCubit, TitleRateState>(
                  buildWhen: (previous, current) {
                    if (current is SuccessTitleRateState) {
                      return current.titleId == title.id;
                    }

                    return true;
                  },
                  builder: (context, state) {
                    bool? _rate = title.rate != null ? (title.rate == 1) : null;

                    if (state is SuccessTitleRateState) {
                      _rate = state.rate;
                    }

                    return CustomIcon(
                      value: _rate,
                      onChanged: canRate
                          ? (bool value) => context
                              .read<TitleRateCubit>()
                              .saveTitleRate(title.id, value ? 1 : -1)
                          : null,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
