import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/register/register_cubit.dart';
import 'package:my_movies_list/data/repositories/user/user_repository_iinterface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/custom/custom_loadin_button.dart';
import 'package:my_movies_list/ui/pages/custom/custom_text_form_field.dart';
import 'package:my_movies_list/ui/pages/home/home_page.dart';

class RegistrationPage extends StatelessWidget {
  static const name = 'registration-page';

  const RegistrationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RegisterCubit>(
      create: (_) => RegisterCubit(getIt.get<UserRepositoryInterface>()),
      child: RegistrationView(),
    );
  }
}

class RegistrationView extends StatelessWidget {
  RegistrationView({Key? key}) : super(key: key);
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterCubit, RegisterState>(
      listener: (BuildContext context, state) {
        if (state == RegisterState.success) {
          Navigator.pushReplacementNamed(context, HomePage.name);
        }
      },
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Informe alguns dados para criar uma conta!',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  const SizedBox(height: 25.0),
                  CustomTextFormField(
                    controller: _nameController,
                    icon: const Icon(Icons.account_circle_outlined),
                    label: 'Nome',
                    obscure: false,
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextFormField(
                    controller: _emailController,
                    icon: const Icon(Icons.email_outlined),
                    label: 'Email',
                    obscure: false,
                  ),
                  const SizedBox(height: 20.0),
                  CustomTextFormField(
                    controller: _passwordController,
                    icon: const Icon(Icons.lock_outline_rounded),
                    label: 'Senha',
                    obscure: true,
                  ),
                  const SizedBox(height: 25.0),
                  BlocBuilder<RegisterCubit, RegisterState>(
                      builder: (context, state) {
                    return CustomLoadingButton(
                      isLoading: state == RegisterState.processingRegister,
                      onPressed: () {
                        context.read<RegisterCubit>().register(
                            _emailController.text,
                            _passwordController.text,
                            _nameController.text);
                      },
                      child: const Text('Criar Conta'),
                    );
                  }),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
