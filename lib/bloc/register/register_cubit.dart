import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/exeptions/user_not_found.dart';
import 'package:my_movies_list/data/repositories/user/user_repository_iinterface.dart';

enum RegisterState {
  initial,
  processingRegister,
  registerFailed,
  userNotFound,
  success
}

class RegisterCubit extends Cubit<RegisterState> {
  final UserRepositoryInterface _userRepository;

  RegisterCubit(this._userRepository) : super(RegisterState.initial);

  Future<void> register(String email, String password, String name) async {
    emit(RegisterState.processingRegister);
    try {
      await _userRepository.register(email, password, name);
      emit(RegisterState.success);
    } on UserNotFoundException {
      emit(RegisterState.userNotFound);
    } on Exception {
      emit(RegisterState.registerFailed);
    }
  }
}
