import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_movies.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';

abstract class TitleState {}

class LoadingAllTitles extends TitleState {}

class LoadingTitlesState extends TitleState {
  final TitleType type;

  LoadingTitlesState(this.type);
}

class TitlesLoadedState extends TitleState {
  final TitleType type;
  final List<TitleModel> titles;

  TitlesLoadedState(this.titles, this.type);
}

class CatalogTitleCubit extends Cubit<TitleState> {
  final TitleRepositoryInterface _repository;

  CatalogTitleCubit(this._repository) : super(LoadingAllTitles());

  Future<void> fetchTitles(List<TitleType> titlesList) async {
    for (final titleType in titlesList) {
      List<TitleModel> titles;
      emit(LoadingTitlesState(titleType));
      if (titleType.isTvShow) {
        titles = await _repository.getTvList(params: titleType.fetchParams);
      } else {
        titles = await _repository.getMovieList(params: titleType.fetchParams);
      }

      emit(TitlesLoadedState(titles, titleType));
    }
  }
}

class TitleType {
  String label;
  Map<String, dynamic> fetchParams;
  bool isTvShow;

  TitleType(this.label, this.fetchParams, this.isTvShow);
}
