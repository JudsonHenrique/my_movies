import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_favorite_model.dart';

import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';

abstract class RatedTitleListState {}

class ProcessingRatedTitleListState implements RatedTitleListState {}

class SuccessRatedTitleListState implements RatedTitleListState {
  List<TitleFavoriteModel> ratings;

  SuccessRatedTitleListState(this.ratings);

  List<TitleFavoriteModel> get props => ratings;
}

class FailRatedTitleListState implements RatedTitleListState {}

class RatedTitleListCubit extends Cubit<RatedTitleListState> {
  final TitleRepositoryInterface _repository;

  RatedTitleListCubit(this._repository)
      : super(ProcessingRatedTitleListState());

  Future<void> getUserFavoriteTitleList(String? userId) async {
    emit(ProcessingRatedTitleListState());

    try {
      var response = await _repository.getUserFavoriteTitleList(userId);
      emit(SuccessRatedTitleListState(response));
    } catch (e) {
      emit(FailRatedTitleListState());
    }
  }
}
