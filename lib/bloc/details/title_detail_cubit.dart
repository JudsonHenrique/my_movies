import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:my_movies_list/data/models/title_detail_model.dart';
import 'package:my_movies_list/data/repositories/title/title_repository_interface.dart';

abstract class TitleDetailState {}

class ProcessingTitleDetailState implements TitleDetailState {}

class SuccessTitleDetailState implements TitleDetailState {
  final TitleDetailModel detail;

  SuccessTitleDetailState(this.detail);

  TitleDetailModel get props => detail;
}

class FailTitleDetailState implements TitleDetailState {}

class TitleDetailCubit extends Cubit<TitleDetailState> {
  final TitleRepositoryInterface _repository;

  TitleDetailCubit(this._repository) : super(ProcessingTitleDetailState());

  Future<void> getTitleDetail(int titleId, {bool isTvShow = false}) async {
    emit(ProcessingTitleDetailState());

    try {
      var response =
          await _repository.getTitleDetail(titleId, isTvShow: isTvShow);
      emit(SuccessTitleDetailState(response!));
    } catch (e) {
      emit(FailTitleDetailState());
    }
  }
}
